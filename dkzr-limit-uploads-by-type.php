<?php
/**
 * Plugin Name: Limit uploads by file type
 * Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=j@dkzr.nl&item_name=dkzr-limit-uploads-by-type+WordPress+plugin&item_number=Joost+de+Keijzer&currency_code=EUR
 * Plugin URI: https://dkzr.nl/
 * Description: Define maximum upload limits by file type.
 * Author: joostdekeijzer
 * Author URI: https://dkzr.nl
 * Update URI: https://api.dkzr.nl/wp/update-check/
 * Composer Package Name: dkzr/limit-uploads-by-type
 * Version: 2.1.0
 * Text Domain: dkzr-limit-uploads-by-type
 * Domain Path: /languages
 */

/**
 * As rule of thumb, for a 2560 x 2560 px JPEG file, 2.5 MB is _large_
 * (tested with Photoshop quality '10' which is their default 'maximum' value).
 * So we keep 1/2560 KB per pixel as reference size.
 */

class dkzrLimitUploadsByFileType {
  protected $settings = [];

  public function __construct() {
    add_action( 'wp_loaded', [ $this, 'wp_loaded' ] );

    add_filter( 'wp_handle_sideload_prefilter', [ $this, 'check_upload_size' ] );
    add_filter( 'wp_handle_upload_prefilter', [ $this, 'check_upload_size' ] );
  }

  public function wp_loaded() {
    $settings = [];

    foreach ( wp_get_mime_types() as $type ) {
      switch( strtok( $type, '/' ) ) {
        case 'image':
          $settings[ $type ] = [
            'size'  => pow( $this->get_threshold(), 2) / 2560, // around 2.5MB in KB for 2560 x 2560 px JPEG image using 1/2560 KB per pixel reference size.
            /* translators: %1$s: Maximum allowed file size; %2$s: Unit of maximum, 'KB' or 'px'; %3$s: File type */
            'error' => __( 'This file is too big. %3$s files must be less than %1$s %2$s in size.', 'dkzr-limit-uploads-by-type' ),
          ];
          break;
        case 'video':
          $settings[ $type ] = [
            'size'  => 0,
            /* translators: %1$s: Maximum allowed file size; %2$s: Unit of maximum, 'KB' or 'px'; %3$s: File type */
            'error' => __( 'Please upload video files to Vimeo, Youtube or another video hosting service and embed it in your page.', 'dkzr-limit-uploads-by-type' ),
          ];
          break;
        case 'audio':
          $settings[ $type ] = [
            'size'  => 0,
            /* translators: %1$s: Maximum allowed file size; %2$s: Unit of maximum, 'KB' or 'px'; %3$s: File type */
            'error' => __( 'Please upload audo files to Soundcloud, Stitcher or another audio hosting service and embed it in your page.', 'dkzr-limit-uploads-by-type' ),
          ];
          break;
      }
    }

    /**
     * Filters DKZR Limit Uploads Settings
     *
     * @param array $settings An array of settings per upload filetype.
     */
    $this->settings = apply_filters( 'dkzr_limit_uploads_settings', $settings );
  }

  public function check_upload_size( $file ) {
    if ( $file['error'] > 0 ) { // There's already an error.
      return $file;
    }

    if ( defined( 'WP_IMPORTING' ) ) {
      return $file;
    }

    $error = false;

    $file_type = $file['type'];
    $file_size = $file['size'];

    if ( isset( $this->settings[ $file_type ] ) && $file_size > ( KB_IN_BYTES * $this->settings[ $file_type ]['size'] ) ) {
      /* translators: %1$s: Maximum allowed file size; %2$s: Unit of maximum, 'KB' or 'px'; %3$s: File type */
      $error = sprintf( $this->settings[ $file_type ]['error'], ( floor( $this->settings[ $file_type ]['size'] / 102.4 ) / 10 ), __( 'MB', 'dkzr-limit-uploads-by-type' ), $file_type );
    }

    if ( 'image' == strtok( $file_type, '/' ) ) {
      $imagesize = wp_getimagesize( $file[ 'tmp_name' ] );
      $threshold = $this->get_threshold( $imagesize );

      if ( ! empty( $imagesize ) && $threshold && ( $imagesize[0] > $threshold || $imagesize[1] > $threshold ) ) {
        $error = sprintf( $this->settings[ $file_type ]['error'], $threshold, __( 'px', 'dkzr-limit-uploads-by-type' ), $file_type );
      }
    }

    /**
     * Filters DKZR Limit Uploads Settings
     *
     * @param bool|string $error Error string or FALSE if there is no error.
     * @param array $file {
     *     Reference to a single element from `$_FILES`.
     *
     *     @type string $name     The original name of the file on the client machine.
     *     @type string $type     The mime type of the file, if the browser provided this information.
     *     @type string $tmp_name The temporary filename of the file in which the uploaded file was stored on the server.
     *     @type int    $size     The size, in bytes, of the uploaded file.
     *     @type int    $error    The error code associated with this file upload.
     * }
     @param array $settings The current plugin settings.
     */
    $error = apply_filters( 'dkzr_limit_uploads_check_error', $error, $file, $this->settings );

    if ( $error ) {
      if ( ! isset( $_POST['html-upload'] ) && ! wp_doing_ajax() ) {
        wp_die( $error . ' <a href="javascript:history.go(-1)">' . __( 'Back' ) . '</a>' );
      } else {
        $file['error'] = $error;
      }
    }

    return $file;
  }

  protected function get_threshold( $imagesize = 0, $file = '', $attachment_id = 0 ) {
    return (int) apply_filters( 'big_image_size_threshold', 2560, $imagesize, $file, $attachment_id );
  }
}
$dkzrLimitUploadsByFileType = new dkzrLimitUploadsByFileType();
