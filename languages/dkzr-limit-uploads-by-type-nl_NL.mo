��          \      �       �      �   *   �      �   l   �   g   i  E   �       �         3        M  p   P  j   �  J   ,     w                                       Back Define maximum upload limits by file type. MB Please upload audo files to Soundcloud, Stitcher or another audio hosting service and embed it in your page. Please upload video files to Vimeo, Youtube or another video hosting service and embed it in your page. This file is too big. %3$s files must be less than %1$s %2$s in size. px Project-Id-Version: Limit uploads by file type
PO-Revision-Date: 2022-03-22 11:19+0100
Last-Translator: joost de keijzer <j@dkzr.nl>
Language-Team: joost de keijzer <j@dkzr.nl>
Language: nl_NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: dkzr-limit-uploads-by-type.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
 Terug Definieer maximale uploadlimieten per bestandstype. MB Upload audo-bestanden naar Soundcloud, Stitcher of een andere audiohostingservice en sluit deze in op uw pagina. Upload videobestanden naar Vimeo, Youtube of een andere videohostingservice en sluit deze in op uw pagina. Dit bestand is te groot. %3$s bestanden moeten kleiner zijn dan %1$s %2$s. px 