=== Limit uploads by file type ===
Contributors: joostdekeijzer
Donate link: https://dkzr.nl/
Tags: media, upload
Requires at least: 5.9
Tested up to: 5.9
Stable tag: v1.2
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Define maximum upload limits by file type.

No admin interface yet, hardcoded values for images, video's and audio.

== Description ==

Limit the upload size for files. Images files may not exeed 5 MB or have a pixel size that exeeds the 'big_image_size_threshold'.

Video and audio files are restricted, users are advised to upload to Vimeo, Youtube, Soundcloud or Stitcher and embed the media to their page.

== Changelog ==

= 1.2 =
* Size limits based on threshold px sizes

= 1.1 =
* More translatable strings
* Dutch translation

= 1.0 =
* Initial Release
